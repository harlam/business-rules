# BUSINESS-RULES

Реализация бизнес-условий и бизнес правил.

Пример:
```php
/* Заказ */
$order = new \stdClass();
$order->price = 1200;
$order->promo = 'promo-code';

/* Условие 1 */
$condition1 = new Condition('Есть Промо-код', '!empty($this->context->promo)');
/* Правило 1 */
$rule1 = new Rule('Скидка 50 руб.', '$this->context->price -= 50;');

/* Условие 2 */
$condition2 = new Condition('Цена больше 1000 руб.', '$this->context->price > 1000');
/* Правило 2 */
$rule2 = new Rule('Скидка 50 руб.', '$this->context->price -= 50;');

/* Набор условий и правил для заказа */
$rulesSet = new RulesSet($order);
$rulesSet->addRule($condition1, $rule1)->addRule($condition2, $rule2);

/* Применить бизнес-правила */
$rulesSet->apply();

/* Информация о выполнении бизнес-правил */
$log = $rulesSet->getLog();

var_dump($order);

var_dump($log);
```

