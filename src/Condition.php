<?php

namespace harlam\BusinessRules;

use harlam\BusinessRules\Interfaces\ConditionInterface;

/**
 * Class Condition
 * @package harlam\BusinessRules
 */
class Condition implements ConditionInterface
{
    protected $name;
    protected $condition;
    protected $context;

    public function __construct(string $name, string $condition)
    {
        $this->name = $name;
        $this->condition = $condition;
    }

    /**
     * Установить контекст
     * @param $context
     * @return ConditionInterface
     */
    public function setContext($context): ConditionInterface
    {
        $this->context = $context;
        return $this;
    }

    /**
     * Имя условия
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Проверить условие
     * @return bool
     */
    public function check(): bool
    {
        return eval('if (' . $this->condition . ') return true; return false;');
    }
}