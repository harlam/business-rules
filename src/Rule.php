<?php

namespace harlam\BusinessRules;

use harlam\BusinessRules\Interfaces\RuleInterface;

/**
 * Class Rule
 * @package harlam\BusinessRules
 */
class Rule implements RuleInterface
{
    protected $name;
    protected $rule;
    protected $context;

    public function __construct(string $name, string $rule)
    {
        $this->name = $name;
        $this->rule = $rule;
    }

    /**
     * Установить контекст
     * @param $context
     * @return RuleInterface
     */
    public function setContext($context): RuleInterface
    {
        $this->context = $context;
        return $this;
    }

    /**
     * Имя правила
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Применить правило
     */
    public function apply(): void
    {
        eval($this->rule);
    }
}