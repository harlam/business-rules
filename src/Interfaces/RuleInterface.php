<?php

namespace harlam\BusinessRules\Interfaces;

/**
 * Интерфейс бизнес-правило
 * @package harlam\BusinessRules\Interfaces
 */
interface RuleInterface
{
    /**
     * Установить контекст
     * @param $context
     * @return RuleInterface
     */
    public function setContext($context): RuleInterface;

    /**
     * Имя правила
     * @return string
     */
    public function getName(): string;

    /**
     * Применить правило
     */
    public function apply(): void;
}