<?php

namespace harlam\BusinessRules\Interfaces;

/**
 * Интерфейс бизнес-условие
 * @package harlam\BusinessRules\Interfaces
 */
interface ConditionInterface
{
    /**
     * Установить контекст
     * @param $context
     * @return ConditionInterface
     */
    public function setContext($context): ConditionInterface;

    /**
     * Имя условия
     * @return string
     */
    public function getName(): string;

    /**
     * Проверить условие
     * @return bool
     */
    public function check(): bool;
}