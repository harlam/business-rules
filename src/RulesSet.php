<?php

namespace harlam\BusinessRules;

use harlam\BusinessRules\Interfaces\ConditionInterface;
use harlam\BusinessRules\Interfaces\RuleInterface;

/**
 * Набор бизнес-правил
 * @package harlam\BusinessRules
 */
class RulesSet
{
    protected $context;
    protected $rules = [];
    protected $log = [];

    public function __construct($context)
    {
        $this->context = $context;
    }

    /**
     * Добавить бизнес-правило
     * @param ConditionInterface $condition
     * @param RuleInterface $rule
     * @return RulesSet
     */
    public function addRule(ConditionInterface $condition, RuleInterface $rule): RulesSet
    {
        $condition->setContext($this->context);
        $rule->setContext($this->context);
        $this->rules[] = ['condition' => $condition, 'rule' => $rule];
        return $this;
    }

    /**
     * Лог применения правил
     * @return array
     */
    public function getLog(): array
    {
        return $this->log;
    }

    /**
     * Применить правила
     */
    public function apply(): void
    {
        $this->log = [];
        $originContext = clone $this->context;
        foreach ($this->rules as $current) {
            try {
                /** @var ConditionInterface $condition */
                $condition = $current['condition'];
                /** @var RuleInterface $rule */
                $rule = $current['rule'];
                if ($condition->check() === true) {
                    $this->log[] = "Выполнено условие '{$condition->getName()}', применено правило '{$rule->getName()}'";
                    $rule->apply();
                } else {
                    $this->log[] = "Условие '{$condition->getName()}' не выполняется";
                }
            } catch (\Throwable $exception) {
                $this->context = $originContext;
                throw new \RuntimeException("Ошибка при применении бизнес-правил: {$exception->getMessage()}");
            }
        }
    }
}